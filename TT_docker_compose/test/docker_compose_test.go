package t

import (
	"os"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/docker"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDockerCompose(t *testing.T) {
	t.Parallel()

	options := &docker.Options{
		ComposeFile: "docker-compose.yaml",
		ProjectName: "myproject-" + random.UniqueId(),
	}

	// Bring up the Docker Compose services
	docker.RunDockerCompose(t, options, &docker.RunOptions{Detached: true})

	// Test the running services
	testApp(t, "http://localhost:3000", "ExpenseCalender_react is running")
	testApp(t, "http://localhost:3001", "FoodOrderApp is running")

	// Tear down the Docker Compose services
	defer docker.RunDockerComposeDown(t, options)
}

func testApp(t *testing.T, url, expectedResponse string) {
	maxRetries := 30
	sleepBetweenRetries := 5 * time.Second

	docker.WaitForHTTPStatus(t, url, 200, maxRetries, sleepBetweenRetries)

	response, err := docker.HTTPGetE(t, url)
	require.NoError(t, err, "Failed to make HTTP GET request to %s", url)

	assert.Contains(t, response, expectedResponse, "Unexpected response from %s", url)
}

func TestMain(m *testing.M) {

	// Run tests
	exitCode := m.Run()

	os.Exit(exitCode)
}
