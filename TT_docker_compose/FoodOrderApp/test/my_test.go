package test

import (
	"github.com/gruntwork-io/terratest/modules/docker"
	"testing"
)

func TestApp(t *testing.T) {

	buildOptions := &docker.BuildOptions{
		Tags: []string{"react-app"},
	}

	docker.Build(t, "../", buildOptions)

	runOptions := &docker.RunOptions{

		OtherOptions: []string{"-p", "3000:3000"},
	}

	// Run the Docker container and get the output

	docker.Run(t, "react-app:latest", runOptions)
}
