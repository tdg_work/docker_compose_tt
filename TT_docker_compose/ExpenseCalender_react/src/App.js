import React,{useState} from 'react';
import Expenses from "./components/Expenses/Expenses";

import NewExpense from './components/NewExpense/NewExpense';

const DUMMY_EXPENSES = [      //expense array of objects which is input to expense compo via props
    {
      id: 'e1',
      title: 'Toilet Paper',
      amount: 94.12,
      date: new Date(2020, 7, 14),
    },
    {
      id: 'e2',
      title: 'New TV',
      amount: 799.49,
      date: new Date(2021, 2, 12)
    },
    {
      id: 'e3',
      title: 'Car Insurance',
      amount: 294.67,
      date: new Date(2021, 2, 28),
    },
    {
      id: 'e4',
      title: 'New Desk (Wooden)',
      amount: 450,
      date: new Date(2021, 5, 12),
    },
  ];
function App() {

  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);
  //const App = () => {
  /*
  const para=document.createElement('p');                 //imperative approach
  para.textContent='This is visible thro regular JS syntax';
  document.getElementById('root').append(para);
*/
  
  //get expenseData from NewExpense compo 
  //and add attribute in NewExpense tag
  const addExpenseHandler = expense => {
      setExpenses(prevExpenses => {
        return [expense, ...prevExpenses];
      });
  }
  //step 4: useState concept ---Form input
  return (                                      //declarative app
    <div>
      
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses items={expenses} />
      
    </div>
  );
  
  
  /*
   //step 3 : import from 'react'       //step 2 preferred
     return React.createElement(
          'div',
          {},
          React.createElement('h2',{},"Hi Mahesh") , 
          React.createElement(Expenses,{items:expenses} )
        );
  */



  /*
  //step 2: composition (children props)
  return (                                      //declarative app
  <div>
    <h2>Hi Mahesh</h2>
    <div>
      <Expenses items={expenses} />
    </div>
  </div>
); 
*/
  /* //step 1
  return (                                      //declarative app
    <div>
      <h2>Hi Mahesh</h2>
      <ExpenseItem                              //added attributes 
          title={expenses[0].title} 
          amount={expenses[0].amount}  
          date={expenses[0].date}>
      </ExpenseItem>
      <ExpenseItem 
          title={expenses[1].title} 
          amount={expenses[1].amount}  
          date={expenses[1].date}>
      </ExpenseItem>
      <ExpenseItem 
          title={expenses[2].title} 
          amount={expenses[2].amount}  
          date={expenses[2].date}>
      </ExpenseItem>
      <ExpenseItem 
          title={expenses[3].title} 
          amount={expenses[3].amount}  
          date={expenses[3].date}>
      </ExpenseItem>
    </div>
  ); 
  */
}

export default App;

