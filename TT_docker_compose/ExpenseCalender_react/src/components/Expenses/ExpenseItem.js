import React from 'react';

import ExpenseDate from './ExpenseDate';
import Card from '../UI/Card';
import './ExpenseItem.css';

const ExpenseItem = (props) => {
  return (
    <li>
      <Card className='expense-item'>
        <ExpenseDate date={props.date} />
        <div className='expense-item__description'>
          <h2>{props.title}</h2>
          <div className='expense-item__price'>${props.amount}</div>
        </div>
      </Card>
    </li>
  );
};



// // //Step 5: *******useState concept to change updated title 
// function ExpenseItem(props) {
//     const [title, setTitle] = useState(props.title);
//     const clickHandler = () => {
//         setTitle('Updated !!');
//         console.log(title);
//     };
//     return (                                         //{} placeholder
//         props.date} />               {title}${props.amount}
//             Change Title
//     );
// }


/*
// 4. created seperate expenseDate.js compo to split this expenseItem.js compo
function ExpenseItem(props) {
    let titles= props.title;
    const clickHandler = () =>{
        titles='Updated';               //title not updated ,so use State concept*******
        console.log("Clicked!!");
    };
    return (                                         //{} placeholder
            props.date}/>               {titles}${props.amount}
            Change Title
    );
}
*/


// // 3. change date format as required
// function ExpenseItem(props) {
//     const month = props.date.toLocaleString('en-US', {month:'long'});
//     const day = props.date.toLocaleString('en-US', {day:'2-digit'});
//     const year = props.date.getFullYear();
    
//     return (                                //{} placeholder
//             <div>                                        
//                 {month}                    {/* extract month date and year here */}
//                 {year}
//                 {day}
//                 { props.title }
//                 ${props.amount}
//             </div>
            
        
//     );
// }



/*
// 2. props concept: data need to input here is store in app compo
// props allows  u to pass data from another component to this compo
function ExpenseItem(props) {
    return (                                         //{} placeholder{props.date.toISOString()}</div>
            <div className="expense-item__description">
                <h2>{props.title}</h2>
                <div className="expense-item__price">${props.amount}</div>
            </div>
        </div>
    );
}
*/

/* // 1. hard coded data but we need dynamic data input
function ExpenseItem() {
    const expenseDate = new Date(2021, 2, 28);    //still hard coded data
    const expenseTitle = 'Car Insurance';
    const expenseAmount = 294.67;
    return (                                         //{} placeholder
        <div className="expense-item">
            <div>{expenseDate.toISOString()}</div>
            <div className="expense-item__description">
                <h2>{expenseTitle}</h2>
                <div className="expense-item__price">${expenseAmount}</div>
            </div>
        </div>
    );
}
*/
export default ExpenseItem;

